import 'package:flutter/material.dart';
import 'package:news_mvvm/utils/theme.dart';
import 'package:news_mvvm/viewmodels/news_list_view_model.dart';
import 'package:news_mvvm/views/news_screen.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'News App',
      theme: light,
      home: MultiProvider(providers: [
        ChangeNotifierProvider(create: (_) => NewsListViewModel())
      ], child: NewsScreen()),
    );
  }
}
