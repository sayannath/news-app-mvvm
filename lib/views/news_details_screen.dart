import 'package:flutter/material.dart';
import 'package:news_mvvm/components/news_image.dart';
import 'package:news_mvvm/utils/config.dart';
import 'package:news_mvvm/viewmodels/news_view_model.dart';

class NewsDetailsScreen extends StatelessWidget {
  final NewsViewModel news;
  NewsDetailsScreen({this.news});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircleAvatar(
              backgroundImage: AssetImage(Config.profileImage),
            ),
            SizedBox(width: 4),
            Container(
              constraints: BoxConstraints(maxWidth: 150),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Author',
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .copyWith(color: Colors.grey),
                  ),
                  Text(
                    news.author ?? 'Unknown',
                    overflow: TextOverflow.ellipsis,
                  )
                ],
              ),
            )
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 30.0, right: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              alignment: Alignment.centerLeft,
              children: [
                Divider(color: Color(0xffff8a30), thickness: 20, height: 80),
                Text(' Headline',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        .copyWith(color: Colors.white))
              ],
            ),
            SizedBox(height: 16),
            Text(news.title,
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(fontWeight: FontWeight.bold, wordSpacing: 3)),
            Text(news.publishedAt,
                style: Theme.of(context)
                    .textTheme
                    .subtitle1
                    .copyWith(color: Colors.grey)),
            SizedBox(height: 24),
            Container(
              height: 200,
              child: NewsImage(imageUrl: news.imageUrl),
            ),
            SizedBox(height: 16),
            Text(news.description,
                style: Theme.of(context)
                    .textTheme
                    .subtitle1
                    .copyWith(wordSpacing: 3)),
            SizedBox(height: 16),
            Text(news.content, style: Theme.of(context).textTheme.subtitle1),
          ],
        ),
      ),
    );
  }
}
