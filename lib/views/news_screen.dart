import 'package:flutter/material.dart';
import 'package:news_mvvm/utils/config.dart';
import 'package:provider/provider.dart';

import 'package:news_mvvm/components/news_card.dart';
import 'package:news_mvvm/viewmodels/news_list_view_model.dart';

class NewsScreen extends StatefulWidget {
  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  @override
  void initState() {
    super.initState();
    Provider.of<NewsListViewModel>(context, listen: false).topHeadlines();
  }

  @override
  Widget build(BuildContext context) {
    var listViewModel = Provider.of<NewsListViewModel>(context);
    return Scaffold(
        appBar: AppBar(
          actions: [popupWidget(context, listViewModel)],
        ),
        //TODO: Implement Drawer
        drawer: Drawer(),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            titleWidget(context),
            dividerWidget(context),
            headlineWidget(context),
            newsWidget(context, listViewModel),
          ],
        ));
  }

  Widget popupWidget(context, var newsList) {
    return PopupMenuButton(
        onSelected: (country) {
          newsList.topHeadlinesByCountry(Config.Countries[country]);
        },
        icon: Icon(Icons.more_vert),
        itemBuilder: (_) {
          return Config.Countries.keys
              .map((e) => PopupMenuItem(value: e, child: Text(e)))
              .toList();
        });
  }

  Widget titleWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0),
      child: Text('News Today', style: Theme.of(context).textTheme.headline3),
    );
  }

  Widget dividerWidget(BuildContext context) {
    return Divider(
        color: Color(0xffff8a30),
        height: 40,
        thickness: 8,
        indent: 30,
        endIndent: 20);
  }

  Widget headlineWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0, top: 16.0, bottom: 16.0),
      child: Text('Headlines',
          style: Theme.of(context)
              .textTheme
              .headline6
              .copyWith(fontWeight: FontWeight.bold)),
    );
  }

  Widget newsWidget(BuildContext context, var newsList) {
    return Expanded(
      child: NewsCard(
        newsListViewModel: newsList.news,
      ),
    );
  }
}
