import 'package:flutter/material.dart';

ThemeData light = ThemeData(
    scaffoldBackgroundColor: Color(0xfffefdfd),
    appBarTheme: AppBarTheme(
        color: Color(0xfffefdfd),
        elevation: 0,
        textTheme: TextTheme(
            headline6:
                TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        iconTheme: IconThemeData(color: Colors.black),
        actionsIconTheme: IconThemeData(color: Colors.black)));
