class Config {
  static const profileImage = 'assets/images/profile.jpeg';
  static const newsImage =
      'https://media.istockphoto.com/vectors/male-hand-holding-megaphone-with-breaking-news-speech-bubble-banner-vector-id1197831888?k=6&m=1197831888&s=612x612&w=0&h=VJvkLPJQ3k3xhN16Y-g7dAGqxmWOHfPkteHwtjYlTSc=';

  static const API_KEY = '08c8b652c6cf408fa6e6875e8d1fef9e';
  static const newsUSA =
      'https://newsapi.org/v2/top-headlines?country=us&apiKey=$API_KEY';

  static const Map<String, String> Countries = {
    "USA": "us",
    "India": "in",
    "Korea": "kr",
    "China": "ch"
  };    

  static String headlinesFor(String country) {
    return 'https://newsapi.org/v2/top-headlines?country=$country&apiKey=$API_KEY';
  }
}
