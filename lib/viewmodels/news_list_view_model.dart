import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:news_mvvm/models/News.dart';
import 'package:news_mvvm/services/news_service.dart';
import 'package:news_mvvm/viewmodels/news_view_model.dart';

enum LoadingStatus { completed, searching, empty }

class NewsListViewModel with ChangeNotifier {
  LoadingStatus loadingStatus = LoadingStatus.empty;
  List<NewsViewModel> news = List<NewsViewModel>();

  void topHeadlinesByCountry(String country) async {
    List<News> newsArticles = await NewsService().getNewsByCountry(country);
    loadingStatus = LoadingStatus.searching;
    notifyListeners();

    this.news = newsArticles
        .map((article) => NewsViewModel(news: article))
        .toList();

    if (this.news.isEmpty) {
      this.loadingStatus = LoadingStatus.empty;
    } else {
      this.loadingStatus = LoadingStatus.completed;
    }
    notifyListeners();
  }

  void topHeadlines() async {
    List<News> newsArticles = await NewsService().getNews();
    loadingStatus = LoadingStatus.searching;
    notifyListeners();

    this.news = newsArticles
        .map((article) => NewsViewModel(news: article))
        .toList();

    if (this.news.isEmpty) {
      this.loadingStatus = LoadingStatus.empty;
    } else {
      this.loadingStatus = LoadingStatus.completed;
    }
    notifyListeners();
  }
}
