import 'package:dio/dio.dart';

import 'package:news_mvvm/models/News.dart';
import 'package:news_mvvm/utils/config.dart';

class NewsService {
  var dio = new Dio();

  // ignore: missing_return
  Future<List<News>> getNewsByCountry(String country) async {
    final response = await dio.get(Config.headlinesFor(country));
    if (response.statusCode == 200) {
     final result = response.data;
      Iterable list = result['articles'];
      return list.map((article) => News.fromJson(article)).toList();
    } else {
      print("API not working!!!");
    }
  }

  // ignore: missing_return
  Future<List<News>> getNews() async {
    final response = await dio.get(Config.newsUSA);
    if (response.statusCode == 200) {
     final result = response.data;
      Iterable list = result['articles'];
      return list.map((article) => News.fromJson(article)).toList();
    } else {
      print("API not working!!!");
    }
  }
}
