import 'package:flutter/material.dart';
import 'package:news_mvvm/components/news_footer.dart';
import 'package:news_mvvm/components/news_image.dart';
import 'package:news_mvvm/utils/routing.dart';

import 'package:news_mvvm/viewmodels/news_view_model.dart';
import 'package:news_mvvm/views/news_details_screen.dart';

class NewsCard extends StatelessWidget {
  final List<NewsViewModel> newsListViewModel;
  NewsCard({this.newsListViewModel});
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemCount: this.newsListViewModel.length,
        itemBuilder: (context, index) {
          var article = newsListViewModel[index];
          //TODO: Replace with InkWell
          return GestureDetector(
            onTap: () async {
              await Routing.makeRouting(context,
                  routeMethod: 'push',
                  newWidget: NewsDetailsScreen(news: article));
            },
            child: GridTile(
              child: Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 12, vertical: 24),
                child: NewsImage(
                  imageUrl: article.imageUrl,
                ),
              ),
              footer: NewsFooter(title: article.title),
            ),
          );
        });
  }
}
