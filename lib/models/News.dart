import 'package:news_mvvm/utils/config.dart';

class News {
  final String title;
  final String author;
  final String description;
  final String urlToImage;
  final String url;
  final String publishedAt;
  final String content;

  News(
      {this.title,
      this.author,
      this.description,
      this.urlToImage,
      this.url,
      this.publishedAt,
      this.content});

  factory News.fromJson(Map<String, dynamic> json) {
    return News(
      title: json['title'],
      author: json['author'],
      description: json["description"] == null ? 'No Description' : json["description"],
      urlToImage: json["urlToImage"] == null ? Config.newsImage : json["urlToImage"],
      url: json['url'],
      publishedAt: json['publishedAt'],
      content: json["content"] == null ? 'No Content' : json["content"],
    );
  }
}
