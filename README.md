# NEWS App

### A flutter news app which follows MVVM structure

## Project Structure

```
.
├── components
│   ├── news_card.dart
│   ├── news_footer.dart
│   └── news_image.dart
├── main.dart
├── models
│   └── News.dart
├── services
│   └── news_service.dart
├── utils
│   ├── config.dart
│   ├── routing.dart
│   └── theme.dart
├── viewmodels
│   ├── news_list_view_model.dart
│   └── news_view_model.dart
└── views
    ├── news_details_screen.dart
    └── news_screen.dart

6 directories, 13 files
```
